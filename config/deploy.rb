# config valid only for current version of Capistrano
lock '3.3.5'

set :application, 'hochtief_payroll'
set :scm, :git
set :repo_url, 'git@bitbucket.org:ostryalabs/payroll_management.git'
set :user, "deployer"
set :deploy_to, "/home/#{fetch(:user)}/apps/#{fetch(:application)}"
set :current_path, "#{fetch(:deploy_to)}/current"
set :shared_path, "#{fetch(:deploy_to)}/shared"
set :deploy_via, :remote_cache
set :branch, 'master'


set :rbenv_type, :user # or :system, depends on your rbenv setup
set :rbenv_ruby, '2.1.5'
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all # default value
set :use_sudo, true
set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }

# set :delayed_job_server_role, :worker
# set :delayed_job_args, "-n 2"
# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
# 

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

# Number of delayed_job workers
# default value: 1
set :delayed_job_workers, 2

# Delayed_job queue or queues
# Set the --queue or --queues option to work from a particular queue.
# default value: nil
set :delayed_job_queues, ['mailer','tracking']

# Specify different pools
# You can use this option multiple times to start different numbers of workers for different queues.
# default value: nil
set :delayed_job_pools, {
    :mailer => 2,
    :tracking => 1,
    :* => 2
}

# Set the roles where the delayed_job process should be started
# default value: :app
set :delayed_job_roles, [:app, :background]

# Set the location of the delayed_job executable
# Can be relative to the release_path or absolute
# default value 'bin'
#set :delayed_job_bin_path, 'script' # for rails 3.x

namespace :deploy do

  # after :restart, :clear_cache do
  #   on roles(:web), in: :groups, limit: 3, wait: 10 do
  #     # Here we can do anything such as:
  #     # within release_path do
  #     #   execute :rake, 'cache:clear'
  #     # end
  #   end
  # end

  # %w[start stop restart].each do |command|
  #   desc "#{command} unicorn and nginx server"
  #   task command do
  #     on roles(:app) do
  #       execute "/etc/init.d/unicorn_#{fetch(:application)} #{command}"
  #       execute :sudo, "service ngix #{command}"
  #     end
  #   end
  # end



  desc "Link Nginx and unicorn services"
  task :link_services do
    on roles(:app) do
      execute :sudo ,"ln -nfs #{fetch(:current_path)}/config/nginx.conf /etc/nginx/sites-enabled/#{fetch(:application)}"
      execute :sudo, "ln -nfs #{fetch(:current_path)}/config/unicorn_init.sh /etc/init.d/unicorn_#{fetch(:application)}"
    end
  end

  before :publishing, "deploy:link_services"

  # task :symlink_config do
  #   on roles(:app) do
  #     execute :ln, "-nfs", "#{fetch(:shared_path)}/config/database.yml #{fetch(:release_path)}/config/database.yml"
  #   end
  # end
  # after :started, "deploy:symlink_config"

  desc "Make sure local git is in sync with remote."
  task :check_revision do
    on roles(:web) do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        puts "WARNING: HEAD is not the same as origin/master"
        puts "Run `git push` to sync changes."
        exit
      end
    end
  end

  before "deploy", "deploy:check_revision"
end
