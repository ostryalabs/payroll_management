root = "/home/deployer/apps/hochtief_payroll/current"
working_directory root
pid "#{root}/tmp/pids/unicorn.pid"
stderr_path "#{root}/log/unicorn.log"
stdout_path "#{root}/log/unicorn.log"

listen "#{root}/tmp/unicorn.hochtief_payroll.sock"
worker_processes 2
timeout 30
