# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

every :day, :at => '5:00pm', :roles => [:app] do
  command "backup perform -t payroll_production"
end

# every 2.minutes, :roles => [:app] do
#   command "backup perform -t payroll_production"
# end


# Learn more: http://github.com/javan/whenever
