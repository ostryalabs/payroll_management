class CreatePayslipMailingFailures < ActiveRecord::Migration
  def change
    create_table :payslip_mailing_failures do |t|
      t.integer :payslip_id
      t.integer :job_run_id
      t.text :error_message

      t.timestamps
    end
  end
end
