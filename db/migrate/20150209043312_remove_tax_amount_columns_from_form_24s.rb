class RemoveTaxAmountColumnsFromForm24s < ActiveRecord::Migration
  def change
    remove_column :form24s, :edu_cess
    remove_column :form24s, :tds
    remove_column :form24s, :surcharge
    remove_column :form24s, :payslips_id
    remove_column :form24s, :total_tax_deposited
  end
end
