class AddRentRelatedFieldsToSalaryTaxes < ActiveRecord::Migration
  def change
    add_column :salary_taxes, :occupancy_type, :string
    add_column :salary_taxes, :employee_home_loan_interest, :integer
    add_column :salary_taxes, :pt_and_wt, :integer
  end
end
