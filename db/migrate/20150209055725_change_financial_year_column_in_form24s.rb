class ChangeFinancialYearColumnInForm24s < ActiveRecord::Migration
  def change
    change_column :form24s, :financial_year, :string
  end
end
