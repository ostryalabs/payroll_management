class RemoveHomeLoanInterestFromSalaryTax < ActiveRecord::Migration
  def change
    remove_column :salary_taxes, :home_loan_interest, :integer
  end
end
