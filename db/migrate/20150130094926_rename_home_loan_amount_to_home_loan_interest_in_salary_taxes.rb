class RenameHomeLoanAmountToHomeLoanInterestInSalaryTaxes < ActiveRecord::Migration
  def change
    rename_column :salary_taxes, :home_loan_amount, :home_loan_interest
  end
end
