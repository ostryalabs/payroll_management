class PayslipMailingJob

  def initialize(scrolled_by, month, year, date)
    @scrolled_by = scrolled_by
    @date = date
    @month = month
    @year = year
    @job_run_id = JobRun.schedule(JobRun::PAYSLIP_MAILING, scrolled_by, date).id
  end

  def perform
    Payslip.approved.in_the_month(@month).in_the_year(@year).having_atleast_basic.each do |payslip| 
      begin
        if payslip.employee_master.email.present?
          PayslipMailer.payslip(payslip).deliver 
          puts "====Sent mail to #{payslip.employee_master.code} #{payslip.employee_master.name} at #{payslip.employee_master.email}"
        else
          puts "====No mail id found for #{payslip.employee_master.code} #{payslip.employee_master.name}"
        end
      rescue Exception => e
        puts "====Exception while sending mail for #{payslip.employee_master.code} #{payslip.employee_master.name}"
        PayslipMailingFailure.create(:payslip_id => payslip.id, :job_run_id => @job_run_id, :error_message => "Class: #{e.class}, Message: #{e.message}")
      end
    end
  end
  
  def success(job)
    JobRun.finish_as_success(@job_run_id)
  end

  def error(job, error)
    JobRun.finish_as_error(@job_run_id)
  end

  def failure(job)
    JobRun.finish_as_failed(@job_run_id)
  end

  def job_run_id
    @job_run_id
  end
  
end
