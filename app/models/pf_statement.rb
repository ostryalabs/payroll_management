class PfStatement < ActiveRecord::Base
  belongs_to :employee_master
  belongs_to :payslip

  scope :sort_on_employee_code, lambda{ includes(:employee_master).order("employee_masters.code")}

  def self.pf_statement_applicable_date(date)
    if Payslip.in_the_current_month(date).count > 0
      date
    else
      date.prev_month
    end
  end

  def new_entrance?
    employee_master.date_of_joining.month == payslip.generated_date.month and employee_master.date_of_joining.year == payslip.generated_date.year
  end

  def exit?
    employee_master.resignation_date.present? and employee_master.resignation_date.month == payslip.generated_date.month and employee_master.resignation_date.year == payslip.generated_date.year
  end
  
end
