class PayslipMailingFailure < ActiveRecord::Base
  belongs_to :payslip
  belongs_to :job_run
end
