class HomeController < ApplicationController
  def index
    @reminders = Reminder.all.select{|rm| rm.active?(session[:transaction_date])}
  end

  def edit_user
    @users = User.all
  end

  def day_ends
    @day_ends = DayEnd.all
    respond_to do |format|
      format.json do
        render :json => @day_ends
      end	
      format.html do
      end
    end
  end

  def days
    respond_to do |format|
      format.json do
        day_end_params = params[:day_ends]
        day_end = DayEnd.find(day_end_params["id"])
        day_end.transaction_date = day_end_params["transaction_date"]
        day_end.save
      end
      format.html do
      end
      render nothing: true
    end
  end
  
  def update
  end

end
