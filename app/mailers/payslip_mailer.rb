class PayslipMailer < ActionMailer::Base
  default from: "commercial@hochtiefindia.in"
  
  def welcome(email)
    mail(to: 'srikanth@ostryalabs.com', subject: "Testing the production mail settings #{Time.now}")
  end

  def payslip(payslip)
    @employee_master = payslip.employee_master
    date = payslip.generated_date
    @month_str = "#{date.strftime('%b')} #{date.strftime('%Y')}"
    attachments["Payslip_#{date.strftime('%b')}_#{date.strftime('%Y')}.pdf"] =  {
      mime_type: 'application/pdf',
      content: payslip.pdf.render
    }
    mail(to: @employee_master.email, subject: "Payslip #{@month_str}")
  end
  
end
